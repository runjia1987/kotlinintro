package sample.model

/**
 * Created by runjia on 17-5-22.
 */
fun main(args: Array<String>) {
    val array = arrayOf('a', 'b', 'c', 'd')
    for (i in array.indices) {
        println("$i : ${array[i]}")
    }

    var s: String? = null
    s?.run { println("not null") } ?: run { println("null") }
}
