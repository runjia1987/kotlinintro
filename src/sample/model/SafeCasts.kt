package sample.model

/**
 * Created by runjia on 17-5-23.
 */
fun main(args:Array<String>) {
    var str = "123"
    var n : Int? = str.toInt()
    println(n)

    str = "abc"
    n = str as? Int  // smart cast, return null if not castbale
    println(n)
}
