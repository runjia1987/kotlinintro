package sample.model

/**
 * Created by runjia on 17-5-22.
 */
data class Pojo (val properties: Map<String, Any?>) {
    val name: String by properties
    val password: Any? by properties
}

fun main(args: Array<String>) {
    var pojo = Pojo(
            mapOf("name" to "Jack",
                    "password" to 123456)
            )
    println("pojo value: ${pojo.name} : ${pojo.password}")
}

