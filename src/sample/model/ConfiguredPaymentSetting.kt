package sample.model

/**
 * Created by runjia on 17-5-19.
 */
data class ConfiguredPaymentSetting(val merchantId: String, var config: Map<String, Map<String, Int>>?,
                                    var keys: List<String>)

fun main(args: Array<String>) {
    val setting = ConfiguredPaymentSetting("2479832747327432", null, listOf("transactionType", "productCode", "bankAcronym"));
    println(setting.keys)

    setting.keys = listOf("transactionType", "productCode", "bankAcronym")

    var number: Int? = 3
    println(number)

    number = null
    println(number)
}

