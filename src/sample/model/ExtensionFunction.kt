package sample.model

/**
 * Created by Jack on 2017/5/24.
 */
fun Int.isOdd(): Boolean {
    return this % 2 == 1
}
fun main(args:Array<String>) {
    var number: Int = 9999
    println(number.isOdd())
}