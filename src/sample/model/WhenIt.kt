package sample.model

import java.util.*

/**
 * Created by runjia on 17-5-22.
 */

fun whenTest(obj : Any?) {
    when (obj) {
        null -> println("is null")
        is String -> println("String type")
        is Date -> println("is java.util.Date")
        in 1..10 -> println("in 1-10 digits")
        !in 1..10 -> println("not in 1-10 digits")
        else -> println("unknown")
    }
}

fun main(args: Array<String>) {
    whenTest(null)
    whenTest("ABC")
    whenTest(Date())
    whenTest(8)
    whenTest(100)
    whenTest('a')
}
