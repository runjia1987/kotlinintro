package sample.model

/**
 * Created by runjia on 17-5-22.
 */
data class Staff (var name: String?, var password: String?) {

}

fun main(args:Array<String>) {
    val staff = Staff("""Jack""", null)

    println(staff?.password)

    // will return if password is null
    var password = staff.password?: return

    // will throw exception if password is null
    var pwd = staff.password ?: throw IllegalArgumentException("password should not be null")
    println(pwd)
}