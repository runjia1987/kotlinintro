package sample.model

/**
 * Created by runjia on 2017/5/22.
 */
fun fib(n: Int): Long = if (n <= 2) 1 else fib(n-1) + fib(n-2)

fun main(args:Array<String>) {
    println(fib(10))
}