package sample.model

import jdk.nashorn.internal.objects.NativeURIError
import java.time.LocalDateTime
import java.util.*
import kotlin.Comparator

/**
 * Created by runjia on 17-5-19.
 */
fun main(args: Array<String>) {
    val map = TreeMap<String, LocalDateTime>()

    map.put("key1", LocalDateTime.now())
    map.put("key2", LocalDateTime.now())

    println("trave map key = 'key1', value = " + map["key1"])

    // Unit is void type
    fun accept(key: String, value: Any): Unit = println(key + """ --- >>> """ + value)

    fun max(list: List<Long>) : Number? = list.maxWith(compareBy { it })

    fun max2(list: List<Long>) : Number? = list.maxBy { it }

    fun max3(list: List<Long>) : Number? = list.maxWith(
            Comparator { o1: Long, o2: Long -> (o2 - o1).toInt() })

    println("get least member: " + max3(listOf(100, 200, 300, 500, 600, 700)))  // get least

    map.forEach { k, v -> accept(k, v) }

    map.filter { entry -> entry.key.equals("key2") }.values.forEach { x -> println(x) }

    map.toSortedMap(compareByDescending { it }).forEach {
        k, v -> accept(k, v)
    }

    println()

    map.toSortedMap(Comparator.comparing(String::toString).reversed()).forEach( ::accept )
}
