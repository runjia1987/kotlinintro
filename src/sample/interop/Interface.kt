package sample.interop

/**
 * Created by runjia on 17-6-19.
 */
interface Intr

class Concrete: Intr

fun main(args:Array<String>) {
    var conc:Concrete = Concrete() // nothing
    println(conc)
}