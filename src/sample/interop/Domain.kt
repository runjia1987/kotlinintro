package sample.interop

import com.google.common.base.Strings
import lombok.Builder
import lombok.NoArgsConstructor

/**
 * Created by runjia on 17-6-5.
 */
@Builder(toBuilder = true)
@NoArgsConstructor
class Domain (val name:String,  var password:String) {

    lateinit var field: String

    fun isNamePresent() = true

    companion object {
        @JvmStatic
        fun isNamePresent2() = true
    }
}

fun main() {
    println(40 xor 40 xor 50)
    val domain = Domain("Jack", "123abc")

    println(Strings.isNullOrEmpty(domain.name))
    println(Strings.isNullOrEmpty(domain.password))

    println(Domain::class.java.`package`.name)

    println("${domain.isNamePresent()} print")

    domain.name.apply{
        println("${this.length}")
    }

    var number = 123
    println(number.toString())

    // ClassCastException
    //println(number as String)
    // println("123" as Int)
}
