package sample.interop;

import com.google.common.base.Strings;
import sample.fun.MySetting;
import sample.fun.Setting;

import java.util.Arrays;

public class DomainJava {

  public static void main(String[] args) {
    Domain domain = new Domain("Jack", "123abc");

    System.out.println(Strings.isNullOrEmpty(domain.getName()));
    System.out.println(Strings.isNullOrEmpty(domain.getPassword()));

    System.out.println(Arrays.toString(Domain.class.getDeclaredMethods()));

    System.out.println(Domain.isNamePresent2());

    Setting setting = new MySetting("123");
    System.out.println(setting.getId());
  }
}
