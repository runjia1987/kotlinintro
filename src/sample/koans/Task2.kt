package sample.koans

/**
 * Created by runjia on 17-5-27.
 */
fun main(args:Array<String>) {

}

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
    var v1: String = ""
    var v2: String? = ""
    override operator fun compareTo(myDate: MyDate):Int {
        return year - myDate.year +  month - myDate.month + dayOfMonth - myDate.dayOfMonth
    }
}

fun compare(date1: MyDate, date2: MyDate) = date1 < date2

class DateRange(val start: MyDate, val endInclusive: MyDate) {
    operator fun contains(d: MyDate):Boolean = d >= start && d<= endInclusive
}

fun checkInRange(date: MyDate, first: MyDate, last: MyDate): Boolean {
    return date in DateRange(first, last)
}

operator fun MyDate.rangeTo(other: MyDate) = DateRange2(this, other)

class DateRange2(override val start: MyDate, override val endInclusive: MyDate): ClosedRange<MyDate>

fun checkInRange2(date: MyDate, first: MyDate, last: MyDate): Boolean {
    return date in first..last
}