package sample.koans

/**
 * Created by runjia on 17-6-1.
 */
// so that the counter property is incremented every time propertyWithCounter is assigned to.
open class PropertyExample() {
    var counter = 0
    var propertyWithCounter: Int? = null
        set(v) {
            field = v
            counter++
        }
    open fun length(): Int = counter
}

val PropertyExample.length: Int get() = length() * 2

fun main(args:Array<String>) {
    var prop = PropertyExample()
    prop.propertyWithCounter = 123
    prop.propertyWithCounter = 321

    println(prop.counter) // expected 2

    println(prop.length)
}

// lazy init
class LazyProperty(val initializer: () -> Int) {
    var value: Int? = null

    val lazy: Int
        get() {
            if(value == null) {
                value = initializer()
            }
            return value!!
        }
}


