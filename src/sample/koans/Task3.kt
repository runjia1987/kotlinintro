package sample.koans

/**
 * Created by runjia on 17-6-1.
 */
data class Employee (val name:String, val age: Int)

fun checkEmployee(employee: Employee): Boolean {
    var (name, age) = employee  // Destructuring Declarations
    return name.isNotEmpty() && age >= 80
}

fun main(args:Array<String>) {
    var employee = Employee("Jack", 100)
    println(checkEmployee(employee))

    employee = Employee("", 100)
    println(checkEmployee(employee))
}