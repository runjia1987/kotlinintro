package sample.koans

import java.util.*

/**
 * Created by runjia on 17-5-27.
 */
fun joinOptions (options: Collection<String>)
        = options.joinToString(prefix = "[", postfix = "]")

fun containsEven(collection: Collection<Int>): Boolean
        = collection.any { it % 2 == 0 }

val month = "(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)"

fun getPattern(): String = month.toString()

data class Person(val _name: String, val _age: Int) {
    val name: String
        get() = _name
    var counter = 0
}

fun main() {
    println(Person("Jack", 100).name)
    println(Person("Jack", 100)._age)
    var person = Person("", 100)
    person.counter = 9
    println(person.counter)

    println(getListSort())

    println(getListSort2())
}

fun sendMessageToClient(
        client: Client?, message: String?, mailer: Mailer){
    var email = client?.personalInfo?.email
    if (email != null && message != null) {
        mailer.sendMessage(email, message)
    }

}
class Client (val personalInfo: PersonalInfo?)
class PersonalInfo (val email: String?)
interface Mailer {
    fun sendMessage(email: String, message: String)
}

fun eval(expr: Expr): Int =
        when (expr) {
            is Num -> expr.value
            is Sum -> eval(expr.left) + eval(expr.right)
            else -> throw IllegalArgumentException("Unknown expression")
        }

interface Expr
class Num(val value: Int) : Expr
class Sum(val left: Expr, val right: Expr) : Expr

fun Int.r(): RationalNumber = RationalNumber(this, 1)
fun Pair<Int, Int>.r(): RationalNumber = RationalNumber(this.first, this.second)

data class RationalNumber(val numerator: Int, val denominator: Int)

fun getListSort(): List<Int> {
    val arrayList = arrayListOf(1, 5, 2)
    Collections.sort(arrayList, compareByDescending { it })
    return arrayList
}

fun getListSort2(): List<Int> {
    val arrayList = arrayListOf(1, 5, 2)
    Collections.sort(arrayList, { x, y -> y - x })
    return arrayList
}

fun getList(): List<Int> {
    return arrayListOf(1, 5, 2).sortedByDescending { it }
}