package sample.koans

/**
 * Created by runjia on 17-6-1.
 */
data class Shop(val name: String, val customers: List<Customer>)

open class Customer(val name: String, val city: City, val orders: List<Order>) {
    override fun toString() = "$name from ${city.name}"

    infix fun buy(s: String) = println("$name buying $s")
}

class Vip(name: String,  city: City,  orders: List<Order>): Customer(name, city, orders)

fun main() {
    Customer("runjia", City("Shanghai"), listOf()) buy "tomatos"
    Vip("VIP runjia", City("Shanghai"), listOf()) buy "tomatos"
}

data class Order(val products: List<Product>, val isDelivered: Boolean)

data class Product(val name: String, val price: Double) {
    override fun toString() = "'$name' for $price"
}

data class City(val name: String) {
    override fun toString() = name
}

// Return a customer whose order count is the highest among all customers
fun Shop.getCustomerWithMaximumNumberOfOrders(): Customer? = customers.maxBy { it.orders.size }

// Return the most expensive product which has been ordered
fun Customer.getMostExpensiveOrderedProduct(): Product? = orders.flatMap { it.products }.toSet().maxBy { it.price }

// Return all products this customer has ordered
val Customer.orderedProducts: Set<Product> get() {
    return orders.flatMap { it.products }.toSet()
}

// Return all products that were ordered by at least one customer
val Shop.allOrderedProducts: Set<Product> get() {
    return customers.flatMap { it.orders.flatMap { it.products }.toSet() }.toSet()
}

////// Predicates
// Return true if all customers are from the given city
fun Shop.checkAllCustomersAreFrom(city: City): Boolean = customers.all { it.city == city }

// Return true if there is at least one customer from the given city
fun Shop.hasCustomerFrom(city: City): Boolean = customers.any { it.city == city}

// Return the number of customers from the given city
fun Shop.countCustomersFrom(city: City): Int = customers.count {it.city == city}

// Return a customer who lives in the given city, or null if there is none
fun Shop.findAnyCustomerFrom(city: City): Customer? = customers.find { it.city == city }

// Return a list of customers, sorted by the ascending number of orders they made
fun Shop.getCustomersSortedByNumberOfOrders(): List<Customer> = customers.sortedBy { it.orders.size }

// Return the sum of prices of all products that a customer has ordered.
// Note: the customer may order the same product for several times.
fun Customer.getTotalOrderPrice(): Double = this.orders.sumByDouble { it.products.sumByDouble { it.price } }

// Return a map of the customers living in each city
fun Shop.groupCustomersByCity(): Map<City, List<Customer>> = customers.groupBy { it.city }

fun test(shop: Shop) {
    shop.groupCustomersByCity().maxBy { it.value.size }?.value?.size

    shop.groupCustomersByCity().values.map { it.size }.max()
}

// Return customers who have more undelivered orders than delivered
fun Shop.getCustomersWithMoreUndeliveredOrdersThanDelivered(): Set<Customer> =
        customers.partition { it.orders.count { !it.isDelivered } > it.orders.count { it.isDelivered } }.first.toSet()

fun destructing() {
    val numbers = listOf(1, 3, -4, 2, -11)
    val (positive, negative) = numbers.partition { it > 0 }
}

// Return the set of products that were ordered by every customer
fun Shop.getSetOfProductsOrderedByEveryCustomer(): Set<Product> {
    val allProducts= customers.flatMap { it.orders.flatMap { it.products } }.toSet()
    return customers.fold(allProducts) {
        orderedP, cust ->
        orderedP.intersect(cust.orders.flatMap { it.products }.toSet() )
    }
}

// Return the most expensive product among all delivered products
// (use the Order.isDelivered flag)
fun Customer.getMostExpensiveDeliveredProduct(): Product? {
    return orders.filter { it.isDelivered }.flatMap { it.products }.toSet().maxBy { it.price }
}

// Return how many times the given product was ordered.
// Note: a customer may order the same product for several times.
fun Shop.getNumberOfTimesProductWasOrdered(product: Product): Int {
    return customers.flatMap { it.orders.flatMap { it.products.filter { it == product } } }.count()
}

