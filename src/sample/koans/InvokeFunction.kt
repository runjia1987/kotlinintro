package sample.koans

/**
 * Created by runjia on 17-6-1.
 */
// any objects can be have invoke() extension
operator fun Int.invoke() {
    println(this)
}

fun main(args:Array<String>) {
    100()
}

class Invokable {
    var numberOfInvocations: Int = 0
        private set
    operator fun invoke(): Invokable {
        this.numberOfInvocations++
        return this
    }
}

fun invokeTwice(invokable: Invokable) = invokable()()
