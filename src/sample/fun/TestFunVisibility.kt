package sample.`fun`

import sample.model.isOdd
import java.time.LocalDate
import java.time.Period

/**
 * Created by Jack on 2017/5/24.
 */
fun Int.isEven(): Boolean {
    return this % 2 == 0
}

val Int.toPeriod: Period get() = Period.ofDays(this)

val Period.ago: LocalDate get() = LocalDate.now() - this

fun main(args:Array<String>){
    var number = 3
    println(number.isOdd())

    number = 88
    println("isOdd: " + number.isOdd())
    println("isEven: " + number.isEven())

    println(2.toPeriod)
    println(2.toPeriod.ago)
}
