package sample.`fun`

interface Setting {
  val id: String
}

data class MySetting(override val id: String): Setting

fun main(args: Array<String>) {
  println(MySetting("123"))
}