package sample.`fun`

/**
 * Created by runjia on 17-5-25.
 */
data class MyBean (val param: String) {
    operator fun invoke(number: Int): String {
        return param + ":" + number
    }
}

fun main(args:Array<String>) {
    println (MyBean("hello Jack")(100))
}


