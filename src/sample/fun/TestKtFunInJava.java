package sample.fun;

import sample.model.ExtensionFunctionKt;
import sample.fun.TestFunVisibilityKt;

/**
 * Created by Jack on 2017/5/24.
 */
public class TestKtFunInJava {
  public static void main(String[] args){
    System.out.println(ExtensionFunctionKt.isOdd(5));

    System.out.println(TestFunVisibilityKt.isEven(6));
  }
}
